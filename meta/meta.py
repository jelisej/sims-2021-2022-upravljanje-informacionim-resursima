import json
from config.config import read_config


def read_meta():
    path = read_config()["meta"]
    with open(path, "r", encoding="utf-8") as file:
        return json.load(file)


def get_meta(data_name, data_type):
    for key, item in read_meta().items():
        if data_type in item and data_name in item[data_type]:
            return key, item


def is_in_meta(data_name, data_type):
    for item in read_meta().values():
        if data_type in item and data_name in item[data_type]:
            return True
    return False


def get_display(data_name, data_type):
    _, meta = get_meta(data_name, data_type)
    return meta[data_type][data_name] if meta else ""


def get_tab_name(data_name, data_type):
    return get_display(data_name, data_type) + " - " + data_type


def get_folders():
    return read_meta()["folders"]


def get_folder_display(folder_name):
    return read_meta()["folders"][folder_name]


def folder_in_meta(folder_name):
    return folder_name in get_folders()
