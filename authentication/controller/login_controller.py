from csv import DictReader
from gui.main_window import MainWindow


class LoginController:
    # , user_id=None, user_password=None, user_registered=None):
    def __init__(self):
        # self.user_id = user_id
        # self.user_password = user_password
        # self.user_registered = user_registered
        pass

    def login(self, model):
        # preuzmemo podatke sa forme za prijavu
        # ucitamo datoteku da proverimo da li postoji takav kao iz forme
        # FIXME:deo obrade datoteke(provere korisnika) izvuci u drugi .py file koji je file handler, a mi samo pozovemo funkciju
        # FIXME:kasnije ce se podaci ucitavati iz baze podataka,a ne datoteke
        with open("data/serial/users.csv", "r", encoding="utf-8") as csvfile:
            reader = DictReader(csvfile)
            for row in reader:
                if row["user_id"] == model.user_id and row["user_password"] == model.user_password:
                    # u zavisnosti od rezultata proslediti rezultat dijalogu
                    # self.start_app(model)
                    return True
        return False
        # TODO: ako je prijava uspesna pozvati sopstvenu metodu start_app

    def password_change(self, new_password):
        ...

    def quit(self):
        ...

    def start_app(self, model=None):
        main_window = MainWindow()
        main_window.show()
