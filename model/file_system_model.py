from PySide2.QtWidgets import QFileSystemModel
from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon
from meta.meta import is_in_meta, get_display, folder_in_meta, get_folder_display


class FileSystemModel(QFileSystemModel):
    def __init__(self, root, parent=None):
        super().__init__(parent)

        self.setRootPath(root)

    def data(self, index, role=Qt.DecorationRole):
        file = self.fileInfo(index)
        name = file.fileName()
        if index.column() == 0 and role == Qt.DisplayRole:
            type = file.dir().dirName()
            if is_in_meta(name, type):
                return get_display(name, type)
            elif file.isDir() and folder_in_meta(name):
                return get_folder_display(name)
        if index.column() == 0 and role == Qt.DecorationRole:
            if file.isDir():
                if name == "files":
                    return QIcon("resources/icons/folder.png")
                elif name == "serial":
                    return QIcon("resources/icons/serial.png")
                elif name == "sequential":
                    return QIcon("resources/icons/sequential.png")
                elif name == "database":
                    return QIcon("resources/icons/database.png")
            else:
                return QIcon("resources/icons/file.png")
        return super().data(index, role)
