import csv
import os.path
from config.config import read_config
from model.information_resource import InformationResource


class SerialFile(InformationResource):
    def __init__(self, name):
        super().__init__(name)

    @property
    def type(self):
        return "serial"

    def read_data(self):
        path = read_config()[self.type]
        if not os.path.exists(path + self.name):
            return []
        with open(path + self.name, "r", encoding="utf-8") as file:
            return [row for row in csv.reader(file)]

    def save_data(self):
        path = read_config()[self.type]
        with open(path + self.name, "w", encoding="utf-8", newline='') as file:
            csv.writer(file).writerows(self.data)
