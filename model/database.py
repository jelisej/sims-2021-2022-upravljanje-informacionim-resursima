from model.information_resource import InformationResource
from PySide2.QtWidgets import QMessageBox
from config.config import read_config
from mysql import connector


class Database(InformationResource):
    def __init__(self, table_name=""):
        self.connection, self.cursor = Database.connect()

        super().__init__(table_name)

    @property
    def type(self):
        return "database"

    def read_data(self):
        query = "SELECT * FROM " + self.name[:-4] + ";"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def save_data(self):
        self.connection.commit()

    def connect():
        config = read_config()
        connection = connector.connect(
            user=config["user"], passwd=config["passwd"], host=config["host"],  db=config["db"])
        cursor = connection.cursor()
        return connection, cursor

    def disconnect(connection, cursor):
        cursor.close()
        connection.close()

    def read_element(self, index):
        return self.data[index]

    def create_element(self, element, parent):
        values = []
        for el in element:
            if el == None:
                values.append("NULL")
            else:
                values.append(f"'{el}'")
        values = ", ".join(values)
        try:
            query = 'INSERT INTO ' + \
                self.name[:-4] + ' VALUES ' + '(' + values + ')' + ';'
            self.cursor.execute(query)
            return super().create_element(element, parent)
        except connector.errors.IntegrityError as error:
            if error.errno == 1048:
                QMessageBox.warning(
                    parent, "Greška", "Vrijednost kolone ne može biti prazna.")
            elif error.errno == 1062:
                QMessageBox.warning(
                    parent, "Greška", "Unijeta vrijednost primarnog ključa je zauzeta.")
            elif error.errno == 1452:
                QMessageBox.warning(
                    parent, "Greška", "Unijeta vrijednost stranog ključa je nepostojeća.")
        return False

    def update_element(self, index, new_element, parent):
        element = self.data[index]
        set_args = []
        where_args = []
        for i, el in enumerate(new_element):
            name = self.meta["attributes"][i]["name"]
            if element[i] != el:
                if new_element[i] == None:
                    set_args.append(f"{name} = NULL")
                else:
                    set_args.append(f"{name} = '{new_element[i]}'")
            if element[i] == new_element[i] and element[i] == None:
                set_args.append(f"{name} = NULL")
            if self.get_primary_key() is not None:
                name = self.meta["attributes"][i]["name"]
                if element[i] != None:
                    where_args.append(f"{name} = '{element[i]}'")
        set_args = ", ".join(set_args)
        where_args = " AND ".join(where_args)
        try:
            query = "UPDATE " + self.name[:-
                                          4] + " SET " + set_args + " WHERE " + where_args + ";"
            self.cursor.execute(query)
            return super().update_element(index, new_element, parent)
        except connector.errors.IntegrityError as error:
            if error.errno == 1048:
                QMessageBox.warning(
                    parent, "Greška", "Vrijednost kolone ne može biti prazna.")
            elif error.errno == 1062:
                QMessageBox.warning(
                    parent, "Greška", "Unijeta vrijednost primarnog ključa je zauzeta.")
            elif error.errno == 1452:
                QMessageBox.warning(
                    parent, "Greška", "Unijeta vrijednost stranog ključa je nepostojeća.")
            else:
                QMessageBox.warning(
                    parent, "Greška", "Vrijednost primarnog ključa se koristi kao strani ključ u drugoj tabeli.")
        return False

    def delete_element(self, index, parent):
        element = self.data[index]
        where_args = (f"id='{element[0]}'")
        try:
            query = "DELETE FROM " + \
                self.name[:-4] + " WHERE " + where_args + ";"
            self.cursor.execute(query)
            super().delete_element(index, parent)
        except connector.errors.IntegrityError:
            QMessageBox.warning(
                parent, "Greška", "Ne možete obrisati entitet čija se vrijednost primarnog ključa koristi kao strani ključ.")

    def get_primary_key(self):
        primary_key = None
        for attribute in self.meta["attributes"]:
            if "primary key" in attribute["type"]:
                primary_key = attribute
        return primary_key
