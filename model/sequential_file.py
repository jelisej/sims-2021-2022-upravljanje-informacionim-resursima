from model.serial_file import SerialFile


class SequentialFile(SerialFile):
    def __init__(self, name):
        super().__init__(name)

    @property
    def type(self):
        return "sequential"

    def save_data(self):
        super().save_data()
