from PySide2.QtCore import QAbstractTableModel, Qt
from model.serial_file import SerialFile
from model.sequential_file import SequentialFile
from model.database import Database


class InformationResourceModel(QAbstractTableModel):
    def __init__(self, data_type, data_name=None, parent=None):
        super().__init__(parent)
        if not data_name:
            return
        if data_type == "serial":
            self.information_resource = SerialFile(data_name)
        elif data_type == "sequential":
            self.information_resource = SequentialFile(data_name)
        elif data_type == "database":
            self.information_resource = Database(data_name)

    def get_element(self, index):
        return self.information_resource.read_element(index.row())

    def rowCount(self, index=None):
        return len(self.information_resource.data)

    def columnCount(self, index=None):
        return len(self.information_resource.get_attribute())

    def data(self, index, role=Qt.DisplayRole):
        for c in range(self.columnCount()):
            if index.column() == c and role == Qt.DisplayRole:
                if str(self.get_element(index)[c]) != "None":
                    return str(self.get_element(index)[c])

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        for c in range(self.columnCount()):
            if section == c and orientation == Qt.Horizontal and role == Qt.DisplayRole:
                return self.information_resource.get_attribute(c)["display"]
