-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: servis
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `servisiranje`
--

DROP TABLE IF EXISTS `servisiranje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servisiranje` (
  `id` int NOT NULL AUTO_INCREMENT,
  `opis_kvara` text NOT NULL,
  `status` text NOT NULL,
  `cena` double DEFAULT NULL,
  `datum_popravke` date DEFAULT NULL,
  `automobil_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_servisiranje_automobil_idx` (`automobil_id`),
  CONSTRAINT `fk_servisiranje_automobil` FOREIGN KEY (`automobil_id`) REFERENCES `automobil` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servisiranje`
--

LOCK TABLES `servisiranje` WRITE;
/*!40000 ALTER TABLE `servisiranje` DISABLE KEYS */;
INSERT INTO `servisiranje` VALUES (1,'Slabije koči','Uspešna popravka',300,'2020-11-05',1),(2,'Redovan servis','Servis obavljen',300,'2021-10-10',2),(3,'Curi ulje','Uspešna popravka',450,'2022-01-17',2),(4,'Ne rade svetla','Na popravci',NULL,NULL,2);
/*!40000 ALTER TABLE `servisiranje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-08 17:45:06
