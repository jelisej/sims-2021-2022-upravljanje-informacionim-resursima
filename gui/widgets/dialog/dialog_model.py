from .dialog import Dialog
from PySide2.QtWidgets import QLabel, QLineEdit, QDateEdit, QPushButton
from PySide2.QtGui import QRegularExpressionValidator
from PySide2.QtCore import QDate


class DialogModel(Dialog):
    def __init__(self, information_resource, parent=None):
        super().__init__(parent)

        self.information_resource = information_resource
        self.attributes = self.information_resource.get_attribute()

        for i, attribute in enumerate(self.attributes):
            label = QLabel(attribute["display"], self)

            if attribute["input"] in ["text", "varchar", "number"]:
                input = QLineEdit(self)
                if attribute["input"] == "varchar":
                    input.setMaxLength(attribute["length"])
                if attribute["name"] == "registarski_broj":
                    input.setValidator(QRegularExpressionValidator(
                        "[A-Z]{2}\\d{5}[0-9]"))
                elif attribute["name"] == "cena":
                    input.setValidator(
                        QRegularExpressionValidator("^[1-9]\d*(\.\d+)?$"))
                elif attribute["input"] == "number":
                    input.setValidator(
                        QRegularExpressionValidator("^[1-9]\d*$"))
            elif attribute["input"] == "date":
                input = QDateEdit(self)
                input.setMaximumDate(QDate.currentDate())
                input.setDisplayFormat("yyyy-MM-dd")

            self.layout().addWidget(label, i, 0)
            self.layout().addWidget(input, i, 1)

        self.button = QPushButton("Potvrdi")
        self.button.clicked.connect(self.action)
        self.layout().addWidget(self.button, i + 1, 1)
