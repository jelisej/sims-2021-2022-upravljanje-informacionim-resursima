from .dialog_model import DialogModel
from PySide2.QtCore import QDate


class UpdateDialog(DialogModel):
    def __init__(self, information_resource, row, parent=None):
        super().__init__(information_resource, parent)

        self.row = row
        self.set_input_value()
        self.setWindowTitle("Izmjena")

    def set_input_value(self):
        values = self.information_resource.read_element(self.row)
        for i, attribute in enumerate(self.attributes):
            widget = self.layout().itemAtPosition(i, 1).widget()
            if attribute["input"] == "date":
                widget.setDate(QDate.fromString(
                    str(values[i]), "yyyy-MM-dd"))
            else:
                widget.setText(str(values[i]))

    def action(self):
        element = []
        for i, attribute in enumerate(self.attributes):
            widget = self.layout().itemAtPosition(i, 1).widget()
            value = widget.text()
            if value == '' or value == 'None' or value == '2000-01-01':
                value = None
            element.append(value)
        if self.information_resource.update_element(self.row, element, self):
            self.close()
