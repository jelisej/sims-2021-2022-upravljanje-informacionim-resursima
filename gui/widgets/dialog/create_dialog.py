from .dialog_model import DialogModel
from PySide2.QtCore import Signal


class CreateDialog(DialogModel):
    created_signal = Signal()

    def __init__(self, information_resource, parent=None):
        super().__init__(information_resource, parent)

        self.setWindowTitle("Dodavanje")

    def action(self):
        element = []
        for i, attribute in enumerate(self.attributes):
            widget = self.layout().itemAtPosition(i, 1).widget()
            value = widget.text()
            if value == '' or value == '2000-01-01' or value == 'None':
                value = None
            element.append(value)
        if self.information_resource.create_element(element, self):
            self.created_signal.emit()
            self.close()
