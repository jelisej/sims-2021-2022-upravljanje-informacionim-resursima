from PySide2.QtWidgets import QDialog, QGridLayout
from PySide2.QtCore import Qt
from PySide2.QtGui import QIcon
from abc import abstractmethod


class Dialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent, Qt.WindowCloseButtonHint)

        self.setWindowIcon(QIcon("resources/icons/logo.ico"))
        self.setLayout(QGridLayout())

    @abstractmethod
    def action(self):
        ...
