from PySide2.QtWidgets import QToolBar, QAction
from PySide2.QtGui import QIcon
from PySide2.QtCore import QSize


class ToolBar(QToolBar):
    def __init__(self, title='', parent=None):
        super().__init__(title, parent)

        self.setIconSize(QSize(30, 30))
        self.setFixedHeight(45)

        self.create_action = QAction(
            QIcon("resources/icons/create.png"), "Create", self)
        self.update_action = QAction(
            QIcon("resources/icons/update.png"), "Update", self)
        self.delete_action = QAction(
            QIcon("resources/icons/delete.png"), "Delete", self)
        self.save_action = QAction(
            QIcon("resources/icons/save.png"), "Save", self)

        self.addAction(self.create_action)
        self.addAction(self.update_action)
        self.addAction(self.delete_action)
        self.addAction(self.save_action)
