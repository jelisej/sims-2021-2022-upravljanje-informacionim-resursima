from PySide2.QtWidgets import QWidget, QVBoxLayout, QMessageBox
from model.information_resource_model import InformationResourceModel
from .information_resource_view import InformationResourceView
from .dialog.create_dialog import CreateDialog
from .dialog.update_dialog import UpdateDialog


class WorkspaceWidget(QWidget):
    def __init__(self, name, type, parent):
        super().__init__(parent)

        self.model = InformationResourceModel(type, name)
        self.view = InformationResourceView(self.model, self)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.view)
        self.setLayout(self.layout)

    def refresh(self):
        self.model.layoutChanged.emit()
        self.model.layoutAboutToBeChanged.emit()

    def create_row(self):
        self.model.layoutAboutToBeChanged.emit()
        dialog = CreateDialog(self.model.information_resource)
        dialog.created_signal.connect(self.refresh)
        dialog.exec_()
        self.model.layoutChanged.emit()

    def update_row(self):
        indexes = self.view.selectionModel().selectedIndexes()
        try:
            index = self.view.proxy_model.mapToSource(indexes[0])
            self.model.layoutAboutToBeChanged.emit()
            dialog = UpdateDialog(
                self.model.information_resource, index.row(), self)
            dialog.exec_()
            self.model.layoutChanged.emit()
        except IndexError:
            QMessageBox().information(self, "Izmjena",
                                      "Molimo vas da odaberete red za izmjenu.")
            return

    def delete_row(self):
        indexes = self.view.selectionModel().selectedIndexes()
        if not len(indexes):
            QMessageBox().information(self, "Brisanje",
                                      "Molimo vas da odaberete red za brisanje.")
            return

        answer = QMessageBox().question(
            self, "Brisanje", "Da li ste sigurni da želite obrisati ovaj red?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if answer == QMessageBox.Yes:
            index = self.view.proxy_model.mapToSource(indexes[0])
            self.model.layoutAboutToBeChanged.emit()
            self.model.information_resource.delete_element(index.row(), self)
            self.model.layoutChanged.emit()
            self.view.clearSelection()
        self.view.clearSelection()
        return

    def save_resource(self):
        self.model.layoutAboutToBeChanged.emit()
        self.model.information_resource.save_data()
        self.model.layoutChanged.emit()
