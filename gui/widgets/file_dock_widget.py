from PySide2.QtWidgets import QDockWidget, QTreeView
from PySide2.QtCore import QDir, Signal
from config.config import read_config
from model.file_system_model import FileSystemModel


class FileDockWidget(QDockWidget):
    clicked_signal = Signal(str, str)

    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        config = read_config()
        self.file_model = FileSystemModel(
            QDir.currentPath() + "/" + config["data"])

        self.tree_view = QTreeView()

        self.tree_view.setModel(self.file_model)
        self.tree_view.setRootIndex(self.file_model.index(
            QDir.currentPath() + "/" + config["data"]))
        self.tree_view.clicked.connect(self.index_clicked)
        self.tree_view.setHeaderHidden(True)
        self.tree_view.hideColumn(1)
        self.tree_view.hideColumn(2)
        self.tree_view.hideColumn(3)
        self.tree_view.hideColumn(4)

        self.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.setWidget(self.tree_view)

    def index_clicked(self, index):
        path = self.file_model.filePath(index)
        if path.find(".sql") != -1:
            self.clicked_signal.emit(path.split("/")[-1], path.split("/")[-2])
        elif path.find(".csv") != -1:
            self.clicked_signal.emit(path.split("/")[-1], path.split("/")[-2])
        return
