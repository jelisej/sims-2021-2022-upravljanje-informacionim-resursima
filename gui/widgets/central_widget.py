from PySide2.QtWidgets import QTabWidget, QMessageBox
from meta.meta import get_tab_name, is_in_meta
from .workspace_widget import WorkspaceWidget


class CentralWidget(QTabWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self.removeTab)

    @property
    def tabs(self):
        ret_tabs = []
        for i in range(self.count()):
            ret_tabs.append(self.tabText(i))
        return ret_tabs

    def get_index(self, tab_name):
        try:
            return self.tabs.index(tab_name)
        except ValueError:
            return None

    def new_tab(self, data_name, data_type):
        tab_name = get_tab_name(data_name, data_type)
        if tab_name not in self.tabs and is_in_meta(data_name, data_type):
            workspace_widget = WorkspaceWidget(data_name, data_type, self)
            self.addTab(workspace_widget, tab_name)
            self.setCurrentIndex(self.currentIndex() + 1)
        else:
            index = self.get_index(tab_name)
            self.setCurrentIndex(index)

    def save_all(self):
        for i in range(self.count()):
            self.widget(i).save_resource()

    def call_create(self):
        try:
            self.widget(self.currentIndex()).create_row()
        except AttributeError:
            QMessageBox().information(self, "Dodavanje",
                                      "Molimo Vas da otvorite tabelu da biste dodali entitet.")

    def call_update(self):
        try:
            self.widget(self.currentIndex()).update_row()
        except AttributeError:
            QMessageBox().information(self, "Izmjena",
                                      "Molimo Vas da otvorite tabelu i odaberete entitet za izmjenu.")

    def call_delete(self):
        try:
            self.widget(self.currentIndex()).delete_row()
        except AttributeError:
            QMessageBox().information(self, "Brisanje",
                                      "Molimo Vas da otvorite tabelu i odaberete entitet za brisanje.")

    def call_save(self):
        try:
            self.widget(self.currentIndex()).save_resource()
        except AttributeError:
            return
