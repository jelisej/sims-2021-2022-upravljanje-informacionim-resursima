from PySide2.QtWidgets import QMenuBar, QMenu, QAction, QMessageBox
from PySide2.QtCore import Signal
from PySide2.QtGui import QIcon


class MenuBar(QMenuBar):
    close_all = Signal()
    save_all = Signal()
    exit = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)

        # kreiranje osnovnih funkcija
        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.help_menu = QMenu("Help", self)

        # povezivanje menija
        self._populate_menues()
        self.triggered.connect(self.actions)

    def _populate_menues(self):
        self.addMenu(self.file_menu)
        # self.file_menu.addAction(QAction("New", self)) # for creating a new file
        # self.file_menu.addAction(QAction("Open", self)) #for opening an existing file
        self.file_menu.addAction(
            QAction(QIcon("resources/icons/close.png"), "Close All", self))
        self.file_menu.addAction(
            QAction(QIcon("resources/icons/save.png"), "Save All", self))
        self.file_menu.addAction(
            QAction(QIcon("resources/icons/exit.png"), "Exit", self))

        self.addMenu(self.edit_menu)
        self.edit_menu.addAction(
            QAction(QIcon("resources/icons/copy.png"), "Copy", self))
        self.edit_menu.addAction(
            QAction(QIcon("resources/icons/paste.png"), "Paste", self))
        self.edit_menu.addAction(
            QAction(QIcon("resources/icons/cut.png"), "Cut", self))

        self.addMenu(self.help_menu)
        self.help_menu.addAction(
            QAction(QIcon("resources/icons/manual.png"), "Manual", self))
        self.help_menu.addAction(
            QAction(QIcon("resources/icons/about.png"), "About", self))

    def actions(self, action):
        command = action.text()
        if command == "Close All":
            self.close_all.emit()
        elif command == "Save All":
            self.save_all.emit()
        elif command == "Exit":
            self.exit.emit()
        # # edit menu
        if command == "Manual":
            QMessageBox.information(
                self, "Manual", 'Uputstvo za upotrebu programskog proizvoda "Rukovalac informacionim resursima"')
        elif command == "About":
            QMessageBox.information(
                self, "About", 'Programski proizvod iz predmeta "Specifikacija i modelovanje softvera"')
