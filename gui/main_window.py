from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt

# lokalno definisane klase
from gui.widgets.menu_bar import MenuBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.central_widget import CentralWidget
from gui.widgets.file_dock_widget import FileDockWidget


class MainWindow(QMainWindow):
    def __init__(self, parent=None, user=None):
        super().__init__(parent)  # poziv super inicijalizatora (QMainWindow)

        # osnovna podesavanja glavnog prozora
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/logo.ico"))
        self.resize(1000, 650)

        # inicijalizacija osnovnih elemenata GUI-ja
        self.menu_bar = MenuBar(self)
        self.tool_bar = ToolBar("Traka alata", self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
        self.file_dock_widget = FileDockWidget("Struktura resursa", self)

        # uvezivanje elemenata GUI-ja
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea,
                           self.file_dock_widget, Qt.Vertical)

        # akcije
        self.menu_bar.close_all.connect(self.central_widget.clear)
        self.menu_bar.save_all.connect(self.central_widget.save_all)
        self.menu_bar.exit.connect(self.close)

        self.tool_bar.create_action.triggered.connect(
            self.central_widget.call_create)
        self.tool_bar.update_action.triggered.connect(
            self.central_widget.call_update)
        self.tool_bar.delete_action.triggered.connect(
            self.central_widget.call_delete)
        self.tool_bar.save_action.triggered.connect(
            self.central_widget.call_save)

        self.file_dock_widget.clicked_signal.connect(
            self.central_widget.new_tab)

        # sacuvavanje prijavljenog korisnika iz dijaloga
        # self.user = user
        # self.status_bar.addWidget(
        #     QLabel("Ulogovani korisnik:" + self.user.user_id.upper()))
