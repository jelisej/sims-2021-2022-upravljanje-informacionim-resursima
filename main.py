import sys
from PySide2.QtWidgets import QApplication

from gui.main_window import MainWindow
from authentication.ui.login_dialog import LoginDialog
from authentication.controller.login_controller import LoginController
from style.style import read_style

if __name__ == "__main__":
    # napravimo QApplication
    application = QApplication(sys.argv)
    # pokrenuti(prikazati) glavni prozor
    # login_controller = LoginController()
    # login = LoginDialog(controller=login_controller)
    # result = login.exec()
    # if result == 1:
    # login_controller.start_app()
    main_window = MainWindow(user=None)
    main_window.show()
    # else:
    #     sys.exit()

    # pokrenuti aplikaciju
    application.setStyleSheet(read_style())
    sys.exit(application.exec_())
